
class ProductManager {

    constructor () {
        this.products = []
    } 
 
    addProduct (title, description, price, thumbnail, stock, code) {
        // verificar que todos los campos estén presentes y que el código no esté duplicado
        // verificar si falta algún dato obligatorio
        // debe generarse un id diferente para cada producto 
    const idProduct = this.products.length + 1

    if (!idProduct || !title || !description || !price || !thumbnail || !stock || !code) {
        console.log("Faltan campos obligatorios para agregar el producto.");
        return; // salir de la función si falta algún dato obligatorio
      }
  
      // verificar si el código ya existe en algún producto
      const codeAlreadyExists = this.products.some(product => product.code === code);
      if (codeAlreadyExists) {
        console.log("El código ya existe en otro producto.");
        return; // salir de la función si el código ya existe
      }
  
      // si todos los datos están presentes y el código no está repetido, agregar el producto
      this.products.push({
        idProduct,
        title,
        description,
        price,
        thumbnail,
        stock,
        code
      });
      console.log("Producto agregado correctamente.");
    }

    getProducts() {
        return this.products;
      }

    getProductById(id) {
        const producto = this.products.find(product => product.idProduct === id);
        if (producto) {
      console.log("Producto encontrado:", producto);
      return producto;
    } else {
      console.log("Not found.");
      return null;
    }
      }
}

// desde aca hacia abajo es la seccion de pruebas

// prueba de la consigna
const productManager = new ProductManager()

console.log("primer llamada de getProducts, en teoria debe ser vacio:",productManager.getProducts())

productManager.addProduct("producto prueba", "Este es un producto prueba", 200, "Sin imagen", "abc123", 25)
productManager.addProduct("producto prueba", "Este es un producto prueba", 200, "Sin imagen", "abc123", 25)

productManager.getProductById(1) // numero de id existente
productManager.getProductById(10) // numero de id ingresado inexistente

// pruebas extra
// ingreso de productos con posibilidades de errores
productManager.addProduct("Producto 1", "Descripción del producto 1", 10, "imagen1.jpg", 100, "ABC123");
productManager.addProduct("Producto 2", "Descripción del producto 2", 20, "imagen2.jpg", 50, "DEF456");
productManager.addProduct("Producto 3", "Descripción del producto 3", 30, "imagen3.jpg", 75, "ABC123"); // este producto tiene el parametro code repetido
productManager.addProduct("Descripción del producto 3", 30, "imagen3.jpg", 75, "ABC123"); // a este producto le falta un parametro

// obtener productos
const productos = productManager.getProducts();
console.log("Productos cargados:", productos);

// sbtener un producto por ID
const productId = 1; // colocar ID de un producto para pruebas
const producto = productManager.getProductById(productId);
if (producto) {
  console.log("Producto encontrado:", producto);
} else {
  console.log("Not found.");
}