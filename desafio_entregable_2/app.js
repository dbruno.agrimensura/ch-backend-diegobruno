const { DiffieHellmanGroup } = require('crypto');
const { runInThisContext } = require('vm');

const fs = require('fs').promises

class ProductManager {

    constructor (pathRoute) {
        this.products = [];
        this.path = pathRoute
        this.idNP = 0
    } 
 
  async addProduct (newProduct) {
        // verificar que todos los campos estén presentes y que el código no esté duplicado
        // verificar si falta algún dato obligatorio
        // debe generarse un id diferente para cada producto 

    const idProduct = this.idNP + 1

    this.idNP = idProduct

    newProduct.id = idProduct

    console.log(newProduct)

    if (!idProduct || !newProduct.title || !newProduct.description || !newProduct.price || !newProduct.thumbnail || !newProduct.stock || !newProduct.code) {
        console.log("Faltan campos obligatorios para agregar el producto.");
        return; // salir de la función si falta algún dato obligatorio
      }
  
      // verificar si el código ya existe en algún producto
      const codeAlreadyExists = this.products.some(product => product.code === newProduct.code);
      if (codeAlreadyExists) {
        console.log("El código ya existe en otro producto.");
        return; // salir de la función si el código ya existe
      }
  
      try {
        let products = await this.getProducts()

        products.push(newProduct)

        await fs.writeFile(this.path, JSON.stringify(products, null, 2))
        console.log("El producto se creo correctamente")
    } catch (error) {
        console.error("Error al crear el producto", error)
    }

    }

    async getProducts() {
        try {
            const data = await fs.readFile(this.path, 'utf8')
            return JSON.parse(data)
        } catch (error) {
            if (error.code == 'ENOENT'){
              return []
            }else{
              throw error
            }
        }
      }

    async getProductById(id) {

      const data = await fs.readFile(this.path, 'utf8')
      const dataJson = JSON.parse(data)
      
        const producto = dataJson.find(product => product.id === id);
        if (producto) {
      console.log("Producto encontrado:", producto);
      return producto;
    } else {
      console.log("Not found.");
      return null;
    }
      }

      async updateProduct(id, modifyData) {

        const data = await fs.readFile(this.path, 'utf8')
        const dataJson = JSON.parse(data)

        let productSelected = await this.getProductById(id) 

        const datosActualizar = Object.keys(modifyData) // ['title','code']

        for (let index = 0; index < datosActualizar.length; index++) {
          const element = datosActualizar[index]; 
          productSelected[element] = modifyData[element]         
        }

        const arrProductosActualizados = dataJson.map((elemento,index)=>{
          if (elemento.id === id) {
            return productSelected
          } else {
            return elemento
          }
        })
        await fs.writeFile(this.path, JSON.stringify(arrProductosActualizados, null, 2))
        console.log("Se modificó exitosamente el siguiente producto:", productSelected);

      }


      async deleteProduct(id) {
        try {
          // Leer el archivo JSON
          const data = await fs.readFile(this.path, 'utf8');
          let dataJson = JSON.parse(data);
  
          // Buscar el producto con el ID proporcionado
          const index = dataJson.findIndex(product => product.id === id);
          if (index !== -1) {
              // Eliminar el producto completo
              dataJson.splice(index, 1);
              console.log(`Producto con ID ${id} eliminado exitosamente.`);
  
              // Escribir los datos actualizados en el archivo JSON
              await fs.writeFile(this.path, JSON.stringify(dataJson, null, 2), 'utf8');
              console.log('Datos actualizados en el archivo JSON.');
          } else {
              console.log(`No se encontró ningún producto con ID ${id}.`);
          }
      } catch (err) {
          console.error('Error:', err);
      }
      }
}

// desde aca hacia abajo es la seccion de pruebas

// prueba de la consigna
const productManager = new ProductManager('ProductList.json')

productManager.addProduct({title:"producto prueba", description:"Este es un producto prueba", price:200, thumbnail:"Sin imagen", code:"abc123", stock:25});
setTimeout(() => {productManager.addProduct({title:"producto prueba1", description:"Este es un producto prueba1", price:200, thumbnail:"Sin imagen1", code:"abc1234", stock:25})}, 1000);
setTimeout(() => {productManager.addProduct({title:"producto prueba2", description:"Este es un producto prueba2", price:200, thumbnail:"Sin imagen2", code:"abc12345", stock:25})}, 2000);

setTimeout(() => {productManager.getProductById(2)}, 3000)
setTimeout(()=> {productManager.deleteProduct(1)},4000); // Reemplaza '123456' con el ID del producto que deseas eliminar
